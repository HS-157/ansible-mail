# ansible-mail V1

Installation d'un serveur de courriel avec OpenSMTPD, Rspamd et Dovecot.

Source :

* <https://poolp.org/posts/2019-12-23/mettre-en-place-un-serveur-de-mail-avec-opensmtpd-dovecot-et-rspamd/>

## Pré-requis

* IP publique
* Nom de domaine
* Gestion de reverse-DNS
* Certificat TLS pour le domaine

## DNS

Au niveau du DNS, il y a plusieurs choses à faire.


### A / AAAA

Il faut avoir un enregistrement A / AAAA pour l'ip du serveur de courriel.

~~~
hsqth.hs-157.moe.	300	IN	A	46.105.183.37
~~~

### MX

Il faut un enregistrement MX, il permet de décrire les différents serveurs de courriel pour la distribution des courriels.

~~~
test.lett.re.		300	IN	MX	0 hsqth.hs-157.moe.
~~~

### SPF

Du SPF, pour dire quels sont les serveurs qui ont droit d'émettre des courriels avec le nom de domaine.

~~~
test.lett.re.		10800	IN	TXT	"v=spf1 mx -all"
~~~


### DKIM

On peut signer les courriels qu'on envoie pour prouver que c'est bien nous, pour cela, il faut générer une clef privée, puis publier la clef publique dans un enregistrement DNS.

~~~
openssl genrsa -out /etc/mail/dkim/test.lett.re.key 1024
openssl rsa -in /etc/mail/dkim/test.lett.re.key -pubout -out /etc/mail/dkim/test.lett.re.pub
~~~

~~~
default._domainkey.test.lett.re. 10800 IN TXT	"v=DKIM1;k=rsa;p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqnIGAIrBm5Ltc0xEHvxtjAka+E/fy2jGlIxShK0MzO4jyjwMck66T+u0/uSPH/mjcT2Gop+MGRlU70Dzf0/JNpxY7cPGhh5DTQxWhB2y0huVbvt7LLM2xdKPIgHwa7RYLvSyGUIjsBQnnRWLD6a49b8Sij9ghCLpOhIdw5e4e9wIDAQAB;"
~~~

### DMARC

En cas d'erreur pour la signature d'un courriel, il faut quoi faire, c'est là que vient le DMARC.

~~~
_dmarc.test.lett.re.	10800	IN	TXT	"v=DMARC1;p=quarantine;pct=10;rua=mailto:postmaster@test.lett.re;"
~~~

## Rspamd

Installation de Rspamd et de Redis pour l'apprentissage des spams de Rspamd.

Configuration de base + configuration pour la signature ainsi que l'accès à Redis.

* Signature des courriels

Par défaut, Rspamd utilise le domaine de deuxième niveau pour signer, pour lui dire de prendre en compte le domaine en entier, il faut rajouter
un ```use_esld = false``` dans la configuration pour la signature DKMI.

## OpenSMTPD

Installation et configuration en utilisant celle dans l'article.

## Dovecot

Installation et copie de la configuration de base.

Modification pour dire d'utiliser les certificats et pour dire où sont les courriels.

## Entrainement de Rspamd

* Modification de la config pour dire d'utiliser du sieve.
* Config pour les plugins à utiliser
* Déploiement des scripts sieve + sh
* Compilation des scripts sieve

## Difficultés

J'ai rencontré plusieurs difficultés pour l'installation du serveur.

* Signature DKMI

Par défaut, Rspamd prend le domaine de second niveau, si on utiliser un domaine de troisième niveau, ça ne marchera pas.
Pour comprendre d'où ça venant, c'était un peu la misaire, car dans les logs, il parle bien du domaine de deuxième niveau, mais
je n'avais pas fait vraiment attention à ça, car la différence entre « lett.re » et « test.lett.re » est minime. Une fois compris
cela, il faut trouver comment changer ce comportement, dans la doc, ils parlent de cette config mais ce n'était pas explicite du tout
sur le nom de la ligne à modifier.

* Auto-apprentissage de Rspamd

L'apprentissage de Rspamd quand on bouge un courriel du dossier « spam » ne marchait pas, le problème était que Rspamd n'accèdait pas à Redis,
il fallut rajouter une configuration pour dire comment il doit se connecter. (merci kolo)


* Accès utilisateur système

L'authentification utilise l'utilisateur système du serveur, cela ne marchait pas, on doit configurer PAM :

  * <https://wiki.archlinux.org/index.php/OpenSMTPD#System_users_authentication_failure>


## À faire

* Alias
* Utilisateurs non système : https://wiki.archlinux.org/index.php/OpenSMTPD#Create_user_accounts
* Courriel /srv/mail
* SMTP TLS (envoie)
* * Ok - Bon port + identification PAM
* Auto-configuration client : https://developer.mozilla.org/en-US/docs/Mozilla/Thunderbird/Autoconfiguration - https://developer.mozilla.org/en-US/docs/Mozilla/Thunderbird/Autoconfiguration/FileFormat/HowTo
* Connexion indésirable
* DKMS courbe
